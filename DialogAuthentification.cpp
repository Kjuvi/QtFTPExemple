#include "DialogAuthentification.h"
#include "ui_DialogAuthentification.h"

#include <QAuthenticator>

DialogAuthentification::DialogAuthentification(QString *authUsername, QString *authPassword, QWidget *parent) : QDialog(parent), ui(new Ui::DialogAuthentification)
{
    ui->setupUi(this);

    this->authUsername = authUsername;
    this->authPassword = authPassword;

    setWindowTitle("Authentication");
}

DialogAuthentification::~DialogAuthentification()
{
    delete ui;
}

void DialogAuthentification::on_pushButtonConnexion_clicked()
{
    authUsername->clear();
    authUsername->append(ui->lineEditUsername->text());

    authPassword->clear();
    authPassword->append(ui->lineEditPassword->text());

    close();
}
