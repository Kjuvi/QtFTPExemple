#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <QObject>

class QNetworkAccessManager;
class QNetworkReply;
class QAuthenticator;


class FileTransfer : public QObject
{
    Q_OBJECT
public:
    explicit FileTransfer(QObject *parent = 0);

    // Public functions
    void download(QString remoteFilepath, QString localSavingPath);
    void upload(QString localFilepath, QString remoteSavingPath);
    void authenticate();

private:
    // Attributs
    QNetworkAccessManager* nam;

    QString* authUsername;
    QString* authPassword;

    bool isDownloaded;
    QString localSavingPath;

    // private functions
    void onFinishedDownload(QNetworkReply* reply);
    void onFinishedUpload(QNetworkReply *reply);

private slots:
    void onFinished(QNetworkReply*);
    void onAuthenticationRequired(QNetworkReply*,QAuthenticator*);

signals:
    void updateProgress(qint64 bytesReceived, qint64 bytesTotal);
};

#endif // CONTROLLER_H
